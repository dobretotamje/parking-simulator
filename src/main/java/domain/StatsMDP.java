package domain;

import org.deeplearning4j.rl4j.mdp.MDP;
import org.deeplearning4j.rl4j.space.ActionSpace;
import org.deeplearning4j.rl4j.space.Encodable;

import java.awt.image.BufferedImage;

public interface StatsMDP<MDPInput extends Encodable, Integer, DiscreteSpace extends ActionSpace<Integer>> extends MDP<MDPInput, Integer, DiscreteSpace> {
    int getCurEpochsCnt();

    int getCurAction();

    float getCurReward();

    BufferedImage getLastInput();

    int getNumAction();
}