package domain;

import com.badlogic.gdx.math.Vector2;

import java.awt.*;

public class Angle {
    private double rads;

    public Angle(double rads) {
        this.rads = rads;
    }

    /**
     * @param source
     * @param target
     * @return computes angle between source point and target point
     */
    public Angle(Point source, Point target) {
        float angle = (float) Math.atan2(target.y - source.getY(), target.x - source.getX());

        if (angle < 0) {
            angle += 2 * Math.PI;
        }

        this.rads = angle;
    }

    public Angle(Vector2 source, Vector2 target) {
        float angle = (float) Math.atan2(target.y - source.y, target.x - source.x);

        if (angle < 0) {
            angle += 2 * Math.PI;
        }

        this.rads = angle;
    }

    public Angle(float alpha, float beta) {
        this.rads = Math.min((2 * Math.PI) - Math.abs(alpha - beta), Math.abs(alpha - beta));
    }

    public double getDegrees() {
        return getRads() * 57.2957795;
    }

    public double getRads() {
        return rads;
    }

    public void addDegrees(double value) {
        this.rads = this.rads + Math.toRadians(value);
    }
}
