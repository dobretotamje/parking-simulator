package domain;

/**
 * Structure holding restore points - location of the car and angle
 */
public class Checkpoint {
    private final Location location;
    private final double radianAngle;

    public Checkpoint(Location location, double radianAngle) {
        this.location = location;
        this.radianAngle = radianAngle;
    }

    public Location getLocation() {
        return location;
    }

    public double getRadianAngle() {
        return radianAngle;
    }
}
