package domain;

import java.awt.*;
import java.util.Map;

public class StatsWidget implements Drawable<StatsWidget> {

    private final int x;
    private final int y;
    private final Map<Integer, String> actionNameMap = Map.of(0,"LEFT", 1, "ACCELERATE", 2, "RIGHT", 3, "BRAKE");
    private long minFPS = Long.MAX_VALUE;

    public StatsWidget(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    @Override
    public void draw(Graphics g) {
        g.setColor(Color.WHITE);
        Font currentFont = g.getFont();
        Font newFont = currentFont.deriveFont(currentFont.getSize() * 2F);
        g.setFont(newFont);
        var epochsStats = "Epoch: " + LearningEnvironment.CAR_MDP.getCurEpochsCnt();
        var rewardStats = "Reward: " + (int) LearningEnvironment.CAR_MDP.getCurReward();
        var actionStats = "Action: " + actionNameMap.get(LearningEnvironment.CAR_MDP.getCurAction());
        g.drawString("RL FPS: " + CarMDP.getAPS(), x, y - 80);
        g.drawString(epochsStats, x, y - 40);
        g.drawString(rewardStats, x, y);
        g.drawString(actionStats, x, y + 40);
        g.drawString("Input: ", x,y + 80);
        g.drawImage(LearningEnvironment.CAR_MDP.getLastInput(), x + 80, y + 60, null);
        var fps = World.getRenderingComponent().getFPS();
        minFPS = Math.min(minFPS, fps);
        g.drawString("FPS: " + fps, x, y + 120);
        g.drawString("Min FPS: " + minFPS, x, y + 160);

    }

    @Override
    public Point getOrigin() {
        return new Point(x, y);
    }
}
