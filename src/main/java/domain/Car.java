package domain;

import com.badlogic.gdx.math.Vector2;
import domain.utils.ImageUtil;
import org.apache.commons.math3.ml.distance.EuclideanDistance;
import org.datavec.image.loader.ImageLoader;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.api.preprocessor.ImagePreProcessingScaler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.geom.AffineTransform;
import java.awt.geom.Path2D;
import java.awt.geom.PathIterator;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static domain.CarMDP.*;
import static java.lang.Math.abs;

public class Car extends KeyAdapter implements Collidable<Car> {

    private static final double BASE_REWARD = 20;
    private static final int SENSOR_OFFSET = 59 * 2;
    private final static ImagePreProcessingScaler IMAGE_PRE_PROCESSING_SCALER = new ImagePreProcessingScaler();
    private final static ImageLoader IMAGE_LOADER = new ImageLoader(20, 20, 3);
    private static final Logger LOGGER = LoggerFactory.getLogger(Level.class);
    private final int xRadius;
    private final int yRadius;
    private final BufferedImage imageResource;
    private Point initialPosition;
    private final EuclideanDistance euclideanDistance = new EuclideanDistance();
    private final BufferedImage carSensorBuffer = new BufferedImage(SENSOR_WIDTH, SENSOR_HEIGHT, BufferedImage.TYPE_3BYTE_BGR);
    private final BufferedImage carSensorBufferOptimized = new BufferedImage(SENSOR_WIDTH_OPTIMIZED, SENSOR_HEIGHT_OPTIMIZED, BufferedImage.TYPE_3BYTE_BGR);
    public double mass = 800; // em kilograma
    public Vec3 position = new Vec3();
    public Vec3 direction = new Vec3();
    public Vec3 velocity = new Vec3();
    private double reward;
    private double deltaTime;
    static private float MIN_SPEED_THRESHOLD = 0.03f;
    private long lastTime = System.currentTimeMillis();
    public Vec3 acceleration = new Vec3();
    public double cBraking = 35;
    public Vec3 fBraking = new Vec3();
    public double engineForce = 0;
    public Vec3 fTraction = new Vec3();
    public double cDrag = 0.4257;
    public Vec3 fDrag = new Vec3();
    public Vec3 fRolingResistence = new Vec3();
    public double cRolingResistence = 12.8;
    public boolean isTurningRight = false;
    public boolean isTurningLeft = false;
    public Vec3 fLongtitudinal = new Vec3();
    double cTyre = 0.8; //0.6
    boolean isBraking = false;
    private float speed;
    private Angle angle;
    private boolean wKeyHold = false;
    private boolean sKeyHold = false;
    private boolean aKeyHold = false;
    private boolean dKeyHold = false;

    public Car(Point currentPosition, Angle angle) {
        try {
            this.position.x = currentPosition.x;
            this.position.y = currentPosition.y;
            this.direction.x = Math.cos(angle.getRads());
            this.direction.y = Math.sin(angle.getRads());
            this.initialPosition = new Point(currentPosition);
            imageResource = ImageIO.read(getClass().getResource("/car.png"));
            xRadius = imageResource.getWidth() / 2;
            yRadius = imageResource.getHeight() / 2;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * @param path stands for bounds path
     * @return parsed bound points from path
     */
    private static List<Point> getBoundPoints(final Path2D path) {
        final PathIterator pi = path.getPathIterator(null);
        final double[] coordinates = new double[22];
        final List<Point> points = new ArrayList<>();
        while (!pi.isDone()) {
            pi.next();
            pi.currentSegment(coordinates);
            final Point currentPoint = new Point((int) coordinates[0], (int) coordinates[1]);
            points.add(currentPoint);
        }
        return points;
    }

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public double getCurPositionX() {
        return position.x;
    }

    public double getCurPositionY() {
        return position.y;
    }

    public Point getInitialPosition() {
        return initialPosition;
    }

    @Override
    public Angle getAngle() {
        return new Angle((Math.atan2(direction.y, direction.x)));
    }

    public void setAngle(Angle angle) {
        this.angle = angle;
    }

    public void addAngle(double angle) {
        this.angle.addDegrees(angle);
    }

    public void setCurrentPosition(double x, double y) {
        position.x = x;
        position.y = y;
    }

    /**
     * @return car sensor origin actual location
     */
    public Point getSensorOrigin() {
        double originX = getCurPositionX() + (SENSOR_OFFSET * Math.cos(getAngle().getRads()));
        double originY = getCurPositionY() + (SENSOR_OFFSET * Math.sin(getAngle().getRads()));
        return new Point((int) originX, (int) originY);
    }

    @Override
    public void draw(Graphics g) {
        update();
        Graphics2D g2d = (Graphics2D) g;
        g2d.setPaint(Color.GREEN);
        final int x = (int) getCurPositionX();
        final int y = (int) getCurPositionY();
        AffineTransform at = AffineTransform.getTranslateInstance(x - xRadius, y - yRadius);
        at.rotate(getAngle().getRads(), xRadius, yRadius);
        g2d.drawImage(imageResource, at, null);
        drawSensorBuffer(g);
    }

    public BufferedImage getSensorInputRGB() {
        return ImageUtil.scale(carSensorBufferOptimized, 40, 40);
    }

    /**
     * @param g context
     *          Method draws sensor buffer for debugging purposes
     */
    public void drawSensorBuffer(Graphics g) {
        final Point sensorOrigin = this.getSensorOrigin();
        Graphics2D graphics2D = carSensorBuffer.createGraphics();
        graphics2D.clearRect(0, 0, SENSOR_WIDTH, SENSOR_HEIGHT);
        AffineTransform sensorTransform = AffineTransform.getTranslateInstance(-sensorOrigin.x + (SENSOR_WIDTH / 2), -sensorOrigin.y + (SENSOR_HEIGHT / 2));
        sensorTransform.rotate(-this.getAngle().getRads(), sensorOrigin.x, sensorOrigin.y);
        graphics2D.drawRenderedImage(World.getFrontBuffer(), sensorTransform);
        final BufferedImage scaledView = ImageUtil.scale(carSensorBuffer, SENSOR_WIDTH_OPTIMIZED, SENSOR_HEIGHT_OPTIMIZED);
        final Graphics2D graphics = carSensorBufferOptimized.createGraphics();
        graphics.drawImage(scaledView, 0, 0, null);
        g.drawImage(carSensorBufferOptimized, 0, 0, null);
    }

    /**
     * @param frontBuffer current graphics context
     * @return car collides or not in given context (color colliding detection)
     */
    public boolean isColliding(BufferedImage frontBuffer) {
        final List<Point> bounds = getBoundPoints((Path2D) getBounds());

        for (int i = 0; i < 4; i++) {
            final Point bound = bounds.get(i);
            if (new Color(frontBuffer.getRGB(bound.x, bound.y)).equals(Color.BLACK)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Point getOrigin() {
        return new Point((int) getCurPositionX(), (int) getCurPositionY());
    }

    @Override
    public Dimension getDimension() {
        return new Dimension(imageResource.getWidth(), imageResource.getHeight());
    }

    /**
     * resets car into last restore point if one exists, otherwise moves him to starting position
     */
    public void reset() {
        reward = 0;
        final Checkpoint restorePoint = getRestorePoint();
        final Location restoreLocation = restorePoint.getLocation();
        this.initialPosition = new Point((int)restoreLocation.getX(), (int)restoreLocation.getY());
        this.setCurrentPosition(restoreLocation.getX(), restoreLocation.getY());
        this.angle = new Angle(restorePoint.getRadianAngle());
        this.direction.x = Math.cos(restorePoint.getRadianAngle());
        this.direction.y = Math.sin(restorePoint.getRadianAngle());
    }

    /**
     * @return converted graphics car sensor input into raw array (as it can be passed to neural network)
     */
    public INDArray getSensorInput() {
        final INDArray indArray = IMAGE_LOADER.toBgr(carSensorBufferOptimized);
        IMAGE_PRE_PROCESSING_SCALER.preProcess(indArray);
        return indArray;
    }

    Vec3 lastStepLocation = new Vec3();
    /**
     * @return actual car reward computed by euclidean distance
     */
    public double getReward() {
        final Location lineLoc = getRestorePoint().getLocation();
        final double lineLocX = lineLoc.getX();
        final double lineLocY = lineLoc.getY();
        var distanceFromCenter = BASE_REWARD - euclideanDistance.compute(
                new double[]{lineLocX, lineLocY},
                new double[]{getCurPositionX(), getCurPositionY()}
        );

        double distanceTraveled;

        distanceTraveled = abs(lastStepLocation.getSize() - position.getSize());

        if (lastStepLocation.getSize() == 0) {
            distanceTraveled = 0;
        }

        lastStepLocation.set(position);

        return distanceTraveled * distanceFromCenter;
    }

    /**
     * @return last restore point computed from road center dividing line
     */
    private Checkpoint getRestorePoint() {

        List<Vector2> raceTrackPoints = World.LEVEL.getRaceTrack().getPoints();
        var raceTrackPointsCnt = raceTrackPoints.size() - 1;

        final int nearestRestorePointIndex = getNearest(raceTrackPoints, getCurPositionX(), getCurPositionY());
        final Vector2 nearestRestorePoint = raceTrackPoints.get(nearestRestorePointIndex);

        final int leftAnglePointIdx = nearestRestorePointIndex < 3 ? raceTrackPointsCnt - (3 - nearestRestorePointIndex) : nearestRestorePointIndex - 3;
        final int rightAnglePointIdx = nearestRestorePointIndex > raceTrackPointsCnt - 3 ? raceTrackPointsCnt - nearestRestorePointIndex : nearestRestorePointIndex + 3;
        final Angle restoreAngle = new Angle(raceTrackPoints.get(leftAnglePointIdx), raceTrackPoints.get(rightAnglePointIdx));

        return new Checkpoint(new Location(nearestRestorePoint.x, nearestRestorePoint.y), restoreAngle.getRads());
    }

    /**
     * @param guidelines list of points
     * @param x          closest position x
     * @param y          closest position y
     * @return closest point from collection
     */
    private int getNearest(List<Vector2> guidelines, double x, double y) {
        double shortestPath = 999999;
        int nearestRestorePointIdx = 0;
        for (int i = 0; i < guidelines.size(); i++) {
            Vector2 guideline = guidelines.get(i);
            final double distance = euclideanDistance.compute(
                    new double[]{x, y},
                    new double[]{guideline.x, guideline.y}
            );
            if (distance < shortestPath) {
                shortestPath = distance;
                nearestRestorePointIdx = i;
            }
        }
        return nearestRestorePointIdx;
    }

    public void turnLeft() {
        isTurningLeft = true;
    }

    public void turnRight() {
        isTurningRight = true;
    }

    public void accelerate() {
        engineForce = 35;
    }

    public void decelerate() {
        isBraking = true;
    }

    private void update() {

        if (wKeyHold) {
            accelerate();
        }
        if (sKeyHold) {
            decelerate();
        }
        if (aKeyHold) {
            turnLeft();
        }
        if (dKeyHold) {
            turnRight();
        }

        deltaTime = (System.currentTimeMillis() - lastTime) / 14.0f;
        lastTime = System.currentTimeMillis();

        if (isTurningRight) {
            double speed = velocity.getSize();
            direction.rotateZ(Math.toRadians(cTyre) *  Math.min(speed, 1 / speed));
        }
        if (isTurningLeft) {
            double speed = velocity.getSize();
            direction.rotateZ(Math.toRadians(-cTyre) *  Math.min(speed, 1 / speed));
        }

        double difAngle = velocity.getRelativeAngleBetween(direction);
        if (!Double.isNaN(difAngle)) {
            velocity.rotateZ(difAngle);
        }

        calculateBraking();
        calculateTraction();
        calculateDrag();
        calculateRolingResistence();
        calculateLongtitudinalForce();
        calculateAcceleration();
        calculateVelocity(deltaTime);

        if (isBraking && MIN_SPEED_THRESHOLD > velocity.getSize()) {
            velocity.scale(0);
        }

        calculatePosition(deltaTime);

        engineForce = 0;
        isBraking = false;
        isTurningRight = false;
        isTurningLeft = false;
    }

    private void calculateBraking() {
        fBraking.set(velocity);
        fBraking.normalize();
        fBraking.scale(-cBraking);
    }

    private void calculateTraction() {
        fTraction.set(direction);
        fTraction.normalize();
        fTraction.scale(engineForce);
    }

    private void calculateDrag() {
        double speed = velocity.getSize();
        fDrag.set(velocity);
        fDrag.scale(speed);
        fDrag.scale(-cDrag);
    }

    private void calculateRolingResistence() {
        fRolingResistence.set(velocity);
        fRolingResistence.scale(-cRolingResistence);
    }

    private void calculateLongtitudinalForce() {
        if (isBraking) {
            fLongtitudinal.set(fBraking);
        } else {
            fLongtitudinal.set(fTraction);
        }
        fLongtitudinal.add(fDrag);
        fLongtitudinal.add(fRolingResistence);
    }

    private void calculateAcceleration() {
        acceleration.set(fLongtitudinal);
        acceleration.scale(1 / mass);
    }

    private void calculateVelocity(double deltaTime) {
        acceleration.scale(1);
        velocity.add(acceleration);
    }

    private void calculatePosition(double deltaTime) {
        velocity.scale(1);
        position.add(velocity);
    }

    @Override
    public void keyPressed(KeyEvent e) {
        String pressedChar = String.valueOf(e.getKeyChar()).toLowerCase();
        if (pressedChar.equals("w")) {
            wKeyHold = true;
        }
        if (pressedChar.equals("a")) {
            aKeyHold = true;
        }
        if (pressedChar.equals("d")) {
            dKeyHold = true;
        }
        if (pressedChar.equals("s")) {
            sKeyHold = true;
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
        String pressedChar = String.valueOf(e.getKeyChar()).toLowerCase();
        if (pressedChar.equals("w")) {
            wKeyHold = false;
        }
        if (pressedChar.equals("a")) {
            aKeyHold = false;
        }
        if (pressedChar.equals("d")) {
            dKeyHold = false;
        }
        if (pressedChar.equals("s")) {
            sKeyHold = false;
        }
    }
}
