package domain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;

import static domain.ParkingSimulator.RENDER_WINDOW_HEIGHT;
import static domain.ParkingSimulator.RENDER_WINDOW_WIDTH;
import static domain.utils.ImageUtil.copyImage;

/**
 * Component that renders each frame into the screen, can be used with loop, or simply by invoking render method.
 */
public class RenderingComponent extends JComponent {

    private static final Logger LOGGER = LoggerFactory.getLogger(RenderingComponent.class);
    private static short MAX_FPS = 125;
    private static long FPS;
    private long deltaTime;
    private final BufferedImage backBuffer = new BufferedImage(RENDER_WINDOW_WIDTH, RENDER_WINDOW_HEIGHT, BufferedImage.TYPE_3BYTE_BGR);
    private BufferedImage frontBuffer;

    public RenderingComponent() {
        this.setSize(RENDER_WINDOW_WIDTH, RENDER_WINDOW_HEIGHT);
    }

    public BufferedImage getFrontBuffer() {
        return frontBuffer;
    }

    public BufferedImage getBackBuffer() {
        return backBuffer;
    }

    public void infiniteLoop() {
        new Thread(() -> {
            while (true) {
                long time = System.currentTimeMillis();
                long lastTime = time;

                render();
                time = (1000 / MAX_FPS) - (System.currentTimeMillis() - time);

                if (time > 0) {
                    try {
                        Thread.sleep(time);
                    } catch (Exception ignored) {
                    }
                }
                deltaTime = System.currentTimeMillis() - lastTime;
                FPS = 1000 / deltaTime;
            }
        }).start();
    }

    public long getFPS() {
        return FPS;
    }

    /**
     * method for invoking (redrawing) screen manually
     */
    public void render() {
        final Graphics backBufferGraphics = backBuffer.getGraphics();
        backBufferGraphics.clearRect(0, 0, RENDER_WINDOW_WIDTH, RENDER_WINDOW_HEIGHT);

        World.LEVEL.draw(backBuffer);
        for (Drawable object : World.OBJECTS) {
            object.draw(backBufferGraphics);
        }

        final Graphics g = getGraphics();
        if (g != null) {
            g.drawImage(backBuffer, 0, 0, this);
            frontBuffer = copyImage(backBuffer);
        }
    }
}
