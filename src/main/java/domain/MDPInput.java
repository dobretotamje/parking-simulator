package domain;

import org.deeplearning4j.rl4j.space.Encodable;
import org.nd4j.linalg.api.ndarray.INDArray;

/**
 * Holds car state - in this case surroundings of the vehicle
 */
public class MDPInput implements Encodable {
    double[] array;
    INDArray data;

    public MDPInput(INDArray screen) {
        this.array = screen.data().asDouble();
        this.data = screen;
    }

    @Override
    public double[] toArray() {
        return array;
    }

    @Override
    public boolean isSkipped() {
        return false;
    }

    @Override
    public INDArray getData() {
        return data;
    }

    @Override
    public Encodable dup() {
        return null;
    }
}
