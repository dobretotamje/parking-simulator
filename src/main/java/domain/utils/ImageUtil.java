package domain.utils;

import org.imgscalr.Scalr;
import org.nd4j.linalg.api.ndarray.INDArray;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;

public class ImageUtil {
    /**
     * @param image
     * @param pathname
     * @param fileIndex Util to save image to file silently, debugging purposes only
     */
    public static void saveToFile(BufferedImage image, String pathname, int fileIndex) {
        try {
            ImageIO.write(image, "png", (new File(pathname + fileIndex + ".png")));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @param image
     * @param newWidth
     * @param newHeight
     * @return scaled image
     */
    public static BufferedImage scale(BufferedImage image, int newWidth, int newHeight) {
        return Scalr.resize(image, Scalr.Method.BALANCED, newWidth, newHeight);
    }

    /**
     * @param indArray
     * @param pathname
     * @param fileIndex
     * fuction used for debugging purposes only!
     */
    public static void saveINDArrayToFile(INDArray indArray, String pathname, int fileIndex) {
        long[] shape = indArray.shape();
        if (shape.length != 4) {
            System.out.println(Arrays.toString(shape));
        }
        long height = shape[2];
        long width = shape[3];
        double[] doubles = indArray.data().asDouble();
        byte[] recostructedBytes = new byte[doubles.length];
        for (int i = 0; i < doubles.length; i++) {
            double d = doubles[i];
            recostructedBytes[i] = (byte) (d * 255.0);
        }
        BufferedImage featureImage = new BufferedImage((int) width, (int) height, BufferedImage.TYPE_BYTE_GRAY);
        featureImage.getRaster().setDataElements(0, 0, (int) width, (int) height, recostructedBytes);
        ImageUtil.saveToFile(featureImage, pathname, fileIndex);
    }

    /**
     * @param source image
     * @return copied image
     */
    public static BufferedImage copyImage(BufferedImage source) {
        BufferedImage b = new BufferedImage(source.getWidth(), source.getHeight(), source.getType());
        Graphics g = b.getGraphics();
        g.drawImage(source, 0, 0, null);
        g.dispose();
        return b;
    }
}
