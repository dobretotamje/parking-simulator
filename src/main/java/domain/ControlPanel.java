package domain;

import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.io.File;
import java.io.IOException;

import static domain.ParkingSimulator.RENDER_WINDOW_HEIGHT;
import static domain.ParkingSimulator.RENDER_WINDOW_WIDTH;

public class ControlPanel extends JPanel {

    private static File WORKING_DIR = new File(System.getProperty("user.dir"));
    private final JFileChooser fileChooser;

    public ControlPanel() {
        addEditTools();
        this.fileChooser = new JFileChooser();
        this.fileChooser.setCurrentDirectory(WORKING_DIR);
        final JFileChooser directoryChooser = new JFileChooser();
        directoryChooser.setCurrentDirectory(WORKING_DIR);
        directoryChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        directoryChooser.setAcceptAllFileFilterUsed(false);
    }

    /**
     * Adds controlling elements to window - such as buttons, layouts ...
     */
    private void addEditTools() {
        final Button train = new Button("Train");
        final Button predict = new Button("Predict");
        final Button loadMap = new Button("Open map");
        final Button loadNeuralNetwork = new Button("Load neural net");
        final Button generateRacetrack = new Button("Generate racetrack");
        final Box horVerBox = Box.createHorizontalBox();

        final Box horVerBoxTurns = Box.createHorizontalBox();

        final BoxLayout panelLayout = new BoxLayout(this, BoxLayout.Y_AXIS);

        this.add(horVerBox);
        this.add(horVerBoxTurns);
        this.add(train);
        this.add(predict);
        this.add(loadMap);
        this.add(loadNeuralNetwork);
        this.add(generateRacetrack);
        this.setLayout(panelLayout);

        loadMap.addActionListener(e -> {
            FileNameExtensionFilter filter = new FileNameExtensionFilter("Select png image map", "png");
            fileChooser.setFileFilter(filter);
            int returnVal = fileChooser.showOpenDialog(null);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File selectedMapFile = fileChooser.getSelectedFile();
                World.LEVEL = Level.load(selectedMapFile.getAbsolutePath());
                World.invokeRenderTick();
            }
        });

        loadNeuralNetwork.addActionListener(e -> {
            try {
                final FileNameExtensionFilter filter = new FileNameExtensionFilter("", "zip");
                fileChooser.setFileFilter(filter);
                int returnVal = fileChooser.showOpenDialog(null);
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    File selectedModel = fileChooser.getSelectedFile();
                    LearningEnvironment.MULTILAYER_NETWORK = MultiLayerNetwork.load(selectedModel, true);
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });

        train.addActionListener(e -> {
            LearningEnvironment.train();
            LearningEnvironment.saveModel();
        });

        predict.addActionListener(e -> {
            LearningEnvironment.predict();
        });

        generateRacetrack.addActionListener(e -> {
            World.nextLevel();
            Checkpoint startPosition = World.LEVEL.getStartPosition();
            final Car car = new Car(
                    new Point((int)startPosition.getLocation().getX(), (int)startPosition.getLocation().getY()),
                    new Angle(startPosition.getRadianAngle())
            );

            World.spawnObject(car);
            World.spawnObject(new StatsWidget(RENDER_WINDOW_WIDTH / 2, RENDER_WINDOW_HEIGHT / 2));

            //LearningEnvironment.CAR_MDP = new CarMDP(car);
            LearningEnvironment.CAR_MDP = new CarMDP(car);
        });
    }
}
