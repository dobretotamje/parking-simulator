package domain;

import org.deeplearning4j.gym.StepReply;
import org.deeplearning4j.rl4j.mdp.MDP;
import org.deeplearning4j.rl4j.space.ArrayObservationSpace;
import org.deeplearning4j.rl4j.space.DiscreteSpace;
import org.deeplearning4j.rl4j.space.ObservationSpace;

import java.awt.image.BufferedImage;

public class CarMDP implements StatsMDP<MDPInput, Integer, DiscreteSpace> {

    public static final int SENSOR_HEIGHT = 240;
    public static final int SENSOR_WIDTH = 240;
    public static final int SENSOR_HEIGHT_OPTIMIZED = 20;
    public static final int SENSOR_WIDTH_OPTIMIZED = 20;
    public static final int NUM_ACTIONS = 3;
    public static final int COLOR_MODEL_DEPTH = 3;
    public static final int[] SHAPE = {3, SENSOR_HEIGHT_OPTIMIZED, SENSOR_WIDTH_OPTIMIZED};
    private static final int ACTION_TURN_LEFT = 0;
    private static final int ACTION_TURN_RIGHT = 2;
    private static final int ACTION_ACCELERATE = 1;
    private static final int ACTION_DECELERATE = 3;
    private static long APS;

    private final DiscreteSpace discreteSpace;
    private final ObservationSpace<MDPInput> observationSpace;
    private final Car car;
    private float curReward = 0;
    private int curEpochsCnt = 0;
    private int curAction = 0;

    public CarMDP(Car car) {
        this.car = car;
        this.discreteSpace = new DiscreteSpace(NUM_ACTIONS);
        this.observationSpace = new ArrayObservationSpace<>(SHAPE);
    }

    public int getCurEpochsCnt() {
        return curEpochsCnt;
    }

    public int getCurAction() {
        return curAction;
    }

    public float getCurReward() {
        return curReward;
    }

    public static long getAPS() {
        return APS;
    }

    @Override
    public BufferedImage getLastInput() {
        return car.getSensorInputRGB();
    }

    @Override
    public int getNumAction() {
        return NUM_ACTIONS;
    }

    public Car getCar() {
        return car;
    }

    /**
     * @return defined shape of input
     */
    public ObservationSpace<MDPInput> getObservationSpace() {
        return observationSpace;
    }

    /**
     * @return defined available actions
     */
    public DiscreteSpace getActionSpace() {
        return discreteSpace;
    }


    /**
     * @return car epoch starting state
     */
    public MDPInput reset() {
        curReward = 0;
        curAction = 0;
        curEpochsCnt++;
        car.reset();
        return new MDPInput(car.getSensorInput());
    }

    public void close() {
    }

    private long lastTime;
    /**
     * @param action next action to be executed
     * @return car state with reward and also contains info if this step ends epoch (isColliding)
     */
    public StepReply<MDPInput> step(Integer action) {
        long time = System.currentTimeMillis();
        long deltaTime = time - lastTime;

        APS = 1000 / deltaTime;
        lastTime = time;

        switch (action) {
            case ACTION_TURN_LEFT:
                car.turnLeft();
                break;
            case ACTION_ACCELERATE:
                car.accelerate();
                break;
            case ACTION_TURN_RIGHT:
                car.turnRight();
                break;
            case ACTION_DECELERATE:
                car.decelerate();
                break;
        }

        final MDPInput screen = new MDPInput(car.getSensorInput());
        var reward = car.getReward();
        //System.out.println("Velocity=" + car.velocity.getSize());
        //System.out.println("Acceleration=" + car.acceleration.getSize());
        curReward += reward;
        //curReward += car.velocity.getSize() / 10;
        //System.out.println(car.velocity.getSize());
        curAction = action;

        return new StepReply<>(screen, reward, World.isColliding(car), null);
    }

    /**
     * @return car crashes or not - so the epoch can be restarted
     */
    public boolean isDone() {
        return World.isColliding(car);
    }

    public MDP<MDPInput, Integer, DiscreteSpace> newInstance() {
        return new CarMDP(car);
    }
}
