package domain;

import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.inputs.InputType;
import org.deeplearning4j.nn.conf.layers.Convolution2D;
import org.deeplearning4j.nn.conf.layers.DenseLayer;
import org.deeplearning4j.nn.conf.layers.OutputLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.rl4j.learning.Learning;
import org.deeplearning4j.rl4j.learning.sync.qlearning.QLearning;
import org.deeplearning4j.rl4j.network.dqn.DQN;
import org.deeplearning4j.rl4j.policy.DQNPolicy;
import org.deeplearning4j.rl4j.space.DiscreteSpace;
import org.deeplearning4j.util.ModelSerializer;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.learning.config.Adam;
import org.nd4j.linalg.lossfunctions.LossFunctions;
import org.nd4j.shade.guava.base.Preconditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;

/**
 * Component holding all the configurations of neural network
 */
public class LearningEnvironment {

    private static final Logger LOGGER = LoggerFactory.getLogger(LearningEnvironment.class);
    private static final int HISTORY_LENGTH = 3;
    public static MultiLayerConfiguration MULTILAYER_CONFIGURATION = new NeuralNetConfiguration.Builder()
            .seed(12345)
            .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
            .l2(0.0)
            .updater((new Adam()))
            .weightInit(WeightInit.XAVIER)
            .l2(0.0)
            .list()
            .layer(0, new Convolution2D.Builder(new int[]{8, 8}).nIn(HISTORY_LENGTH).nOut(16).stride(new int[]{4, 4}).activation(Activation.RELU).build())
            .layer(1, new Convolution2D.Builder(new int[]{4, 4}).nOut(32).stride(new int[]{2, 2}).activation(Activation.RELU).build())
            .layer(2, new DenseLayer.Builder().nOut(256).activation(Activation.RELU).build())
            .layer(3, new OutputLayer.Builder(LossFunctions.LossFunction.MSE).activation(Activation.IDENTITY).nOut(CarMDP.NUM_ACTIONS).build())
            .setInputType(InputType.convolutional(CarMDP.SENSOR_HEIGHT_OPTIMIZED, CarMDP.SENSOR_WIDTH_OPTIMIZED, CarMDP.COLOR_MODEL_DEPTH))
            .build();

    public static QLearning.QLConfiguration QL_CONFIGURATION =
            new QLearning.QLConfiguration(
                    123,      //Random seed
                    8000000,    //Max step By epoch
                    200000,  //Max step
                    200000,  //Max size of experience replay
                    10,       //size of batches
                    5000,    //target update (hard)
                    500,      //num step noop warmup
                    0.1,      //reward scaling
                    0.99,     //gamma
                    100.0,    //td-error clipping
                    0.01f,     //min epsilon
                    200000,   //num step for eps greedy anneal
                    true      //double-dqn
            );
    public static MultiLayerNetwork MULTILAYER_NETWORK = new MultiLayerNetwork(MULTILAYER_CONFIGURATION);
    public static StatsMDP<MDPInput, Integer, DiscreteSpace> CAR_MDP;
    private static QLearningDiscreteConvExtended<MDPInput> Q_LEARNING_DISCRETE_CONV;
    private static DQN DQN;

    private static short PREDICT_STEPS_CNT = 10000;

    public static void train() {
        DQN = new DQN<>(MULTILAYER_NETWORK);
        Q_LEARNING_DISCRETE_CONV = new QLearningDiscreteConvExtended<>(CAR_MDP, DQN, QL_CONFIGURATION.toLearningConfiguration());
        Q_LEARNING_DISCRETE_CONV.train();
    }

    public static void predict() {
        MULTILAYER_NETWORK.init();
        var curStep = 0;
        var state = CAR_MDP.reset().data;
        var reshaped = state.reshape(1L, state.shape()[0], state.shape()[1], state.shape()[2]);
        var predict = MULTILAYER_NETWORK.output(reshaped);
        Integer maxAction = Learning.getMaxAction(predict);
        while (curStep < PREDICT_STEPS_CNT) {
            var stepReply = CAR_MDP.step(maxAction);
            state = stepReply.isDone() ? CAR_MDP.reset().data : CAR_MDP.step(maxAction).getObservation().data;
            reshaped = state.reshape(1L, state.shape()[0], state.shape()[1], state.shape()[2]);
            predict = MULTILAYER_NETWORK.output(reshaped);
            maxAction = Learning.getMaxAction(predict);
            curStep++;
        }
    }

    public static void saveModel() {
        try {
            ModelSerializer.writeModel(getNeuralNet(), new File("trained_model.zip"), true);
        } catch (IOException e) {
            LOGGER.error("Something failed while saving model", e);
        }
    }

    public static MultiLayerNetwork getNeuralNet() {
        final MultiLayerNetwork neuralNetwork = (MultiLayerNetwork) Q_LEARNING_DISCRETE_CONV.getNeuralNet().getNeuralNetworks()[0];
        Preconditions.checkNotNull(neuralNetwork, "Neural network not found. You need to train one either load from disk.");
        return neuralNetwork;
    }

    public void savePolicy() {
        final DQNPolicy<MDPInput> policy = Q_LEARNING_DISCRETE_CONV.getPolicy();
        try {
            policy.save("policy");
        } catch (IOException e) {
            LOGGER.error("Something failed while saving policy", e);
        }
    }

}
