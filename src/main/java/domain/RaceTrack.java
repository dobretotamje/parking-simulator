package domain;

import com.badlogic.gdx.math.Vector2;

import java.awt.*;
import java.util.List;

public class RaceTrack {

    private final List<Vector2> points;

    public RaceTrack(List<Vector2> points, List<Stroke> widthMap, Vector2[] convexHull) {
        this.points = points;
        this.widthMap = widthMap;
        this.convexHull = convexHull;
    }

    public List<Stroke> getWidthMap() {
        return widthMap;
    }

    private final List<Stroke> widthMap;

    public List<Vector2> getPoints() {
        return points;
    }

    public Vector2[] getConvexHull() {
        return convexHull;
    }

    private final Vector2[] convexHull;
}
