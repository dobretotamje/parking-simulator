package domain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.swing.*;
import java.awt.*;

import static javax.swing.WindowConstants.EXIT_ON_CLOSE;

public class ParkingSimulator {

    private static final Logger LOGGER = LoggerFactory.getLogger(ParkingSimulator.class);
    public static int RENDER_WINDOW_WIDTH = 1191 + 300;
    public static int RENDER_WINDOW_HEIGHT = 600 + 150;


    public ParkingSimulator() {
        final JFrame renderingFrame = new JFrame();
        renderingFrame.setSize(RENDER_WINDOW_WIDTH + 150, RENDER_WINDOW_HEIGHT);
        renderingFrame.setTitle("Parking simulator");
        renderingFrame.setResizable(false);
        renderingFrame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        renderingFrame.add(World.getRenderingComponent());
        renderingFrame.setFocusable(true);

        final Container contentPane = renderingFrame.getContentPane();
        contentPane.setLayout(new BorderLayout());
        contentPane.setSize(150, 600);
        contentPane.add(new ControlPanel(), BorderLayout.EAST);

        renderingFrame.show();

        Checkpoint startPosition = World.LEVEL.getStartPosition();

        final Car car = new Car(
                new Point((int) startPosition.getLocation().getX(), (int) startPosition.getLocation().getY()),
                new Angle(startPosition.getRadianAngle())
        );

        renderingFrame.addKeyListener(car);

        //LearningEnvironment.CAR_MDP = new CarMDP(car);
        LearningEnvironment.CAR_MDP = new CarMDP(car);

        World.spawnObject(car);
        World.spawnObject(new StatsWidget(RENDER_WINDOW_WIDTH / 2, RENDER_WINDOW_HEIGHT / 2));
    }
}
