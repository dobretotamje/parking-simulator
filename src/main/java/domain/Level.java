package domain;

import com.badlogic.gdx.math.Vector2;
import domain.utils.ImageUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.Path2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Random;

/**
 * Holds graphics levels, also includes function for loading level from path (image)
 */
public class Level {
    private static final Logger LOGGER = LoggerFactory.getLogger(Level.class);
    private final BufferedImage level;
    private final Random random = new Random();

    public RaceTrack getRaceTrack() {
        return raceTrack;
    }

    private final RaceTrack raceTrack;
    private final String mapName;

    private Level(BufferedImage level, String mapName, RaceTrack raceTrack) {
        this.level = level;
        this.raceTrack = raceTrack;
        this.mapName = mapName;
    }

    public static Level load(String mapPath) {
        try {
            final File file = new File(mapPath);
            return new Level(ImageIO.read(file), file.getName(), null);
        } catch (IOException e) {
            LOGGER.error("Failed loading map", e);
            return null;
        }
    }

    public static Level random(int width, int height) {
        return new Level(null, null, RacetrackGenerator.generate(width, height));
    }

    public String getMapName() {
        return mapName;
    }

    public Checkpoint getStartPosition() {
        if (raceTrack == null) {
            throw new RuntimeException("Cannot retrieve start location - no racetrack has been loaded yet");
        }
        List<Vector2> points = raceTrack.getPoints();
        var pointsCnt = points.size();
        final Random random = new Random();
        var randPointIndex = random.nextInt(pointsCnt - 6) + 3;
        var randomPosition = points.get(randPointIndex);

        return new Checkpoint(
                new Location(randomPosition.x, randomPosition.y),
                new Angle(points.get(randPointIndex - 3), points.get(randPointIndex + 3)).getRads());
    }

    BufferedImage cachedMap = null;

    void draw(BufferedImage bufferedImage) {
        Graphics graphics = bufferedImage.getGraphics();
        if (level != null) graphics.drawImage(level, 0, 0, null);
        if (raceTrack != null) {
            Graphics2D g2d = (Graphics2D) graphics;
            if (cachedMap == null) {
                g2d.setColor(Color.WHITE);
                List<Vector2> points = raceTrack.getPoints().subList(0, raceTrack.getPoints().size());
                List<Stroke> widthMap = raceTrack.getWidthMap();

                for (int i = 0; i < points.size() - 1; i++) {
                    Path2D path = new Path2D.Float();
                    Vector2 vector2 = points.get(i);
                    Vector2 vector22 = points.get(i + 1);
                    path.moveTo(vector2.x, vector2.y);
                    path.lineTo(vector22.x, vector22.y);
                    g2d.setStroke(widthMap.get(i));
                    g2d.draw(path);
                }

                cachedMap = ImageUtil.copyImage(bufferedImage);
            }

            g2d.drawImage(cachedMap, null, 0, 0);
        }
    }
}
