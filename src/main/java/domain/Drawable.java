package domain;

import java.awt.*;

public interface Drawable<T> {

    void draw(Graphics g);

    Point getOrigin();
}
