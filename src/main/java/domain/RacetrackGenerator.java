package domain;

import com.badlogic.gdx.math.CatmullRomSpline;
import com.badlogic.gdx.math.ConvexHull;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.FloatArray;
import org.nd4j.shade.guava.collect.Iterables;

import java.awt.*;
import java.awt.geom.Path2D;
import java.util.*;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class RacetrackGenerator {

    private static final int WIDTH_BOUND_INDENT = 100;
    private static final int HEIGHT_BOUND_INDENT = 100;
    private static final Random RANDOM = new Random();


    public static RaceTrack generate(int width, int height) {
        FloatArray dataset = genRandomPoints(width, height);

        var polygon = new ConvexHull().computePolygon(dataset.items, 0, dataset.size, false);
        var polygonVec2 = floatArray2Vector2(polygon);
        var polygonDistict = Arrays.copyOfRange(polygonVec2, 0, polygonVec2.length - 1);

        var polygonPushedApart = push(polygonDistict);

        Vector2[] noisedPolygon = addNoise(polygonPushedApart);
        FloatArray noisedPolygonFloatArray = vector2ToFloatArray(noisedPolygon);
        var polygon2 = new ConvexHull().computePolygon(noisedPolygonFloatArray.items, 0, noisedPolygonFloatArray.size, false);
        var polygonVec22 = floatArray2Vector2(polygon2);
        var polygonDistict2 = Arrays.copyOfRange(polygonVec22, 0, polygonVec22.length - 1);

        for (int i = 0; i < 10; ++i) {
            polygonDistict2 = fixAngles(polygonDistict2);
            polygonDistict2 = pushApart(polygonDistict2);
        }

        //so far the track is generated
        List<Vector2> points = calcTrackVertices(polygonDistict2);

        //now generate width map
        final List<Stroke> widthMap = new ArrayList<>();
        int curStrokeWidth = 90;
        int maxStrokeWidth = 100;
        int minStrokeWidth = 80;

        for (int i = 0; i < points.size() - 1; i++) {
            Path2D path = new Path2D.Float();
            Vector2 vector2 = points.get(i);
            Vector2 vector22 = points.get(i + 1);
            path.moveTo(vector2.x, vector2.y);
            path.lineTo(vector22.x, vector22.y);
            int randomAddition = RANDOM.nextInt(5) - 2;
            curStrokeWidth += randomAddition;
            if (curStrokeWidth > maxStrokeWidth) {
                curStrokeWidth = maxStrokeWidth;
            } else if (curStrokeWidth < minStrokeWidth) {
                curStrokeWidth = minStrokeWidth;
            }
            widthMap.add(new BasicStroke(curStrokeWidth, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
        }

        return new RaceTrack(
                points,
                widthMap,
                polygonVec22
        );
    }

    private static Vector2[] floatArray2Vector2(FloatArray floatArray) {
        var vectors = new Vector2[floatArray.size / 2];
        for (int i = 0, vectorsIdx = 0; i < floatArray.size; i += 2, vectorsIdx++) {
            vectors[vectorsIdx] = new Vector2(floatArray.get(i), floatArray.get(i + 1));
        }
        return vectors;
    }

    private static FloatArray vector2ToFloatArray(Vector2[] vector2s) {
        var floatArray = new FloatArray(vector2s.length * 2);
        for (Vector2 vec2 : vector2s) {
            floatArray.add(vec2.x, vec2.y);
        }
        return floatArray;
    }

    private static FloatArray genRandomPoints(int width, int height) {
        var random = new Random();
        int pointCount = random.nextInt(10) + 50; //we'll have a total of 10 to 20 points
        FloatArray points = new FloatArray(pointCount * 2);
        for (int i = 0; i < pointCount; ++i) {
            float x = random.nextInt(width - (WIDTH_BOUND_INDENT * 2)) + WIDTH_BOUND_INDENT;
            float y = random.nextInt(height - (HEIGHT_BOUND_INDENT * 2)) + HEIGHT_BOUND_INDENT;
            points.add(x, y);
        }

        return points;
    }

    private static List<Vector2> calcTrackVertices(Vector2[] dataSet) {
        var step = 2;
        var thickness = 1f;
        var middleVertices = new ArrayList<Vector2>();

        for (float i = 0; i <= 1.0f; ) {
            var p = CatmullRomSpline.calculate(new Vector2(), i, dataSet, true, new Vector2());
            var deriv = CatmullRomSpline.derivative(new Vector2(), i, dataSet, true, new Vector2());
            float len = deriv.len();
            i += step / len;
            deriv.scl(1.0f / len);
            deriv.set(-deriv.y, deriv.x);
            deriv.scl(thickness);
            Vector2 v0 = new Vector2();
            v0.set(p).add(deriv);
            middleVertices.add(v0);

            if (i > 1.0f) {
                break;
            }
        }
        middleVertices.add(Iterables.getFirst(middleVertices, null));
        return middleVertices;
    }

    private static Vector2[] addNoise(Vector2[] dataSet) {
        Vector2[] rSet = new Vector2[dataSet.length * 2];
        Vector2 disp = new Vector2();
        float difficulty = 1f; //the closer the value is to 0, the harder the track should be. Grows exponentially.
        float maxDisp = 50f; // Again, this may change to fit your units.
        for (int i = 0; i < dataSet.length; ++i) {
            float dispLen = (float) Math.pow(MathUtils.random(0.0f, 1.0f), difficulty) * maxDisp;
            disp.set(0, 1);
            disp.rotateDeg(MathUtils.random(0.0f, 1.0f) * 360);
            disp.scl(dispLen);
            rSet[i * 2] = dataSet[i];
            rSet[i * 2 + 1] = new Vector2(dataSet[i]);
            rSet[i * 2 + 1].add(dataSet[(i + 1) % dataSet.length]).scl(0.5f).add(disp);
            //Explaining: a mid point can be found with (dataSet[i]+dataSet[i+1])/2.
            //Then we just add the displacement.
        }
        dataSet = rSet;
        int pushIterations = 3;
        //push apart again, so we can stabilize the points distances.
        for (int i = 0; i < pushIterations; ++i) {
            dataSet = pushApart(dataSet);
        }

        return dataSet;
    }

    private static Vector2[] pushApart(Vector2[] dataSet) {
        float dst = 110;
        float dst2 = dst * dst;
        Map<Integer, AtomicInteger> clumpingMap = new HashMap<>();
        List<Vector2> datasetList = new ArrayList<>(Arrays.asList(dataSet));
        boolean isFirstIter = true;
        while (isFirstIter || !clumpingMap.isEmpty()) {
            isFirstIter = false;
            clumpingMap.clear();
            for (int i = 0; i < datasetList.size(); ++i) {
                for (int j = i + 1; j < datasetList.size(); ++j) {
                    if (datasetList.get(i).dst2(datasetList.get(j)) < dst2) {
                        clumpingMap.computeIfAbsent(i, (x) -> new AtomicInteger()).incrementAndGet();
                        clumpingMap.computeIfAbsent(j, (x) -> new AtomicInteger()).incrementAndGet();
                    }
//                    float hx = dataSet[j].x - dataSet[i].x;
//                    float hy = dataSet[j].y - dataSet[i].y;
//                    float hl = (float) Math.sqrt(hx * hx + hy * hy);
//                    hx /= hl;
//                    hy /= hl;
//                    float dif = dst - hl;
//                    hx *= dif;
//                    hy *= dif;
//                    dataSet[j].x += hx;
//                    dataSet[j].y += hy;
//                    dataSet[i].x -= hx;
//                    dataSet[i].y -= hy;
                }
            }
            if (!clumpingMap.isEmpty()) {
                var mostClumpedPoint = clumpingMap.entrySet().stream().max((key, value) -> key.getValue().get()).orElseThrow();
                datasetList.remove(mostClumpedPoint.getKey().intValue());
            }
        }

        Vector2[] newDataSet = new Vector2[datasetList.size()];
        datasetList.toArray(newDataSet);
        return newDataSet;
    }

    private static Vector2[] push(Vector2[] dataSet) {
        int pushIterations = 3;
        for (int i = 0; i < pushIterations; ++i) {
            dataSet = pushApart(dataSet);
        }

        return dataSet;
    }

    private static Vector2[] fixAngles(Vector2[] dataSet) {
        for (int i = 0; i < dataSet.length; ++i) {
            int previous = (i - 1 < 0) ? dataSet.length - 1 : i - 1;
            int next = (i + 1) % dataSet.length;
            float px = dataSet[i].x - dataSet[previous].x;
            float py = dataSet[i].y - dataSet[previous].y;
            float pl = (float) Math.sqrt(px * px + py * py);
            px /= pl;
            py /= pl;

            float nx = dataSet[i].x - dataSet[next].x;
            float ny = dataSet[i].y - dataSet[next].y;
            nx = -nx;
            ny = -ny;
            float nl = (float) Math.sqrt(nx * nx + ny * ny);
            nx /= nl;
            ny /= nl;
            //I got a vector going to the next and to the previous points, normalised.

            float a = MathUtils.atan2(px * ny - py * nx, px * nx + py * ny); // perp dot product between the previous and next point

            if (Math.abs(a * MathUtils.radDeg) <= 100)
                continue;

            float nA = 100 * Math.signum(a) * MathUtils.degRad;
            float diff = nA - a;
            float cos = (float) Math.cos(diff);
            float sin = (float) Math.sin(diff);
            float newX = nx * cos - ny * sin;
            float newY = nx * sin + ny * cos;
            newX *= nl;
            newY *= nl;
            dataSet[next].x = dataSet[i].x + newX;
            dataSet[next].y = dataSet[i].y + newY;
            //I got the difference between the current angle and 100degrees, and built a new vector that puts the next point at 100 degrees.
        }
        return dataSet;
    }
}
