package domain;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;

public interface Collidable<T> extends Drawable<T> {

    Dimension getDimension();

    Angle getAngle();

    default Shape getBounds() {
        final Point origin = getOrigin();
        final Dimension dimension = getDimension();
        final int width = (int) dimension.getWidth();
        final int height = (int) dimension.getHeight();
        final int x = (int) (origin.getX() - width / 2);
        final int y = (int) (origin.getY() - height / 2);
        final Rectangle rectangle = new Rectangle(x, y, width, height);
        final AffineTransform at = new AffineTransform();
        at.rotate(getAngle().getRads(), origin.getX(), origin.getY());
        return at.createTransformedShape(rectangle);
    }

    boolean isColliding(BufferedImage screen);
}
