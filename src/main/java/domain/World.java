package domain;

import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

import static domain.ParkingSimulator.RENDER_WINDOW_HEIGHT;
import static domain.ParkingSimulator.RENDER_WINDOW_WIDTH;

/**
 * Class that encapsulates level and rendering component, all the function should be used from this class
 */
public class World {
    public static List<Drawable> OBJECTS = new ArrayList<>();
    //public static Level LEVEL = Level.load("default-map3.png");
    public static Level LEVEL = Level.random(RENDER_WINDOW_WIDTH, RENDER_WINDOW_HEIGHT);

    private static RenderingComponent RENDERING_COMPONENT = new RenderingComponent();

    static {
        //RENDERING_COMPONENT.show();
        RENDERING_COMPONENT.infiniteLoop();
    }

    public static void spawnObject(Drawable object) {
        OBJECTS.add(object);
    }

    public static void nextLevel() {
        OBJECTS.clear();
        LEVEL = Level.random(RENDER_WINDOW_WIDTH, RENDER_WINDOW_HEIGHT);
    }

    /**
     * @param object
     * @param <T>
     * @return if object is  colliding in current context
     */
    public static <T> boolean isColliding(Collidable<T> object) {
        final BufferedImage frontBuffer = RENDERING_COMPONENT.getFrontBuffer();
        return object.isColliding(frontBuffer);
    }

    /**
     * @return currently display graphics context
     */
    public static BufferedImage getFrontBuffer() {
        return RENDERING_COMPONENT.getFrontBuffer();
    }

    /**
     * @return currently prepared graphics context (not rendered yet)
     */
    public static BufferedImage getBackBuffer() {
        return RENDERING_COMPONENT.getBackBuffer();
    }

    public static RenderingComponent getRenderingComponent() {
        return RENDERING_COMPONENT;
    }

    public static void invokeRenderTick() {
        RENDERING_COMPONENT.render();
    }
}
